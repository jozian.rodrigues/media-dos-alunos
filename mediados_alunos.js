function inicio() {
    let nota1 = 0.0, nota2 = 0.0, nota3 = 0.0, media = 0.0;

    // Solicitação de dados de entrada para o usuário digitar/informar
    console.log("Digite a 1º nota do aluno: ");
    nota1 = parseFloat(prompt());

    console.log("Digite a 2º nota do aluno: ");
    nota2 = parseFloat(prompt());

    console.log("Digite a 3º nota do aluno: ");
    nota3 = parseFloat(prompt());

    // Operação: Cálculos
    media = (nota1 + nota2 + nota3) / 3;
    console.log("A média obtida foi: " + media + "\nO Resultado é...\n");

    // Estruturas condicionais simples para saída/impressão de informação em tela ao usuário (Resultado)
    if (media >= 0 && media < 4) {
        console.log("\nReprovado.\n");
    } else if (media >= 4 && media < 7) {
        console.log("\nExame Final.\n");
    } else {
        console.log("\nAprovado.\n");
    }
}

// Chamada da função
inicio();